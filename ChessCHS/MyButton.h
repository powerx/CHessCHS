#pragma once

#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// CMyButton window



class CButtonCH : public CButton
{
public:
    // 用于保存棋盘中按键的枚举类型
    enum MyButtonImage{
        BUTTONIMAGE_MIN         =IDB_MAIN_BUTTONMIN,
        BUTTONIMAGE_CLOSE       =IDB_MAIN_BUTTONCLOSE,
        BUTTONIMAGE_EXIT        =IDB_MAIN_BUTTONEXIT,
        BUTTONIMAGE_TOOL        =IDB_MAIN_BUTTONTOOL,
        BUTTONIMAGE_DATING      =IDB_MAIN_BUTTONDATING,
        BUTTONIMAGE_CONFIG      =IDB_MAIN_BUTTONCONFIG,
        BUTTONIMAGE_SEND        =IDB_MAIN_BUTTONSEND,
        BUTTONIMAGE_INVATEFRIEND=IDB_MAIN_BUTTONINVATEFRIEND,

        BUTTONIMAGE_DAPU        =IDB_MAIN_BUTTONDAPU,
        BUTTONIMAGE_HUIQI       =IDB_MAIN_BUTTONHUIQI,
        BUTTONIMAGE_QIUHE       =IDB_MAIN_BUTTONQIUHE,
        BUTTONIMAGE_START       =IDB_MAIN_BUTTONSTART,
        BUTTONIMAGE_RENSHU      =IDB_MAIN_BUTTONRENSHU
    };

    enum MyButtonKind{
        BUTTONKIND_CLOSE=3,  BUTTONKIND_CONFIG=3, BUTTONKIND_TOOL=3, 
        BUTTONKIND_START=4,  BUTTONKIND_DATING=3, BUTTONKIND_EXIT=3,
        BUTTONKIND_HUIQI=4,  BUTTONKIND_MIN=3,    BUTTONKIND_DAPU=4,
        BUTTONKIND_QIUHE=4,  BUTTONKIND_RENSHU=4, BUTTONKIND_SEND=3,
        BUTTONKIND_INVATEFRIEND=3
    };

private:
	enum MyButtonStatus{
		BUTTONSTATUS_DEFAULT = 0,
        BUTTONSTATUS_OVER    = 1,
        BUTTONSTATUS_DOWN    = 2,
        BUTTONSTATUS_DISABLE = 3
	};

    struct MyButtonInfo
    {
        CPoint         buttonCPoint;
        MyButtonImage  buttonImage;
        MyButtonKind   buttonKind;
        MyButtonStatus buttonStatus;
    };

// Construction
public:
    CButtonCH(const MyButtonInfo buttonInfo);

// Attributes
public:

// Operations
public:
    BOOL EnableWindow();
    BOOL EnableWindow(BOOL bEnableButton);
	void ModifyPlace();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMyButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CButtonCH();

	// Generated message map functions
protected:
	//{{AFX_MSG(CMyButton)
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
    bool m_bTracking;
    BOOL m_bEnableButton;
    MyButtonInfo m_MyButtonInfo;

public:
    static MyButtonInfo s_MyButtonInfoMin;
    static MyButtonInfo s_MyButtonInfoClose;
    static MyButtonInfo s_MyButtonInfoInvatefriend;
    static MyButtonInfo s_MyButtonInfoExit;
    static MyButtonInfo s_MyButtonInfoTool;
    static MyButtonInfo s_MyButtonInfoDaTing;
    static MyButtonInfo s_MyButtonInfoConfig;
    static MyButtonInfo s_MyButtonInfoSend;

    static MyButtonInfo s_MyButtonInfoDaPu;
    static MyButtonInfo s_MyButtonInfoHuiQi;
    static MyButtonInfo s_MyButtonInfoQuiHe;
    static MyButtonInfo s_MyButtonInfoStart;
    static MyButtonInfo s_MyButtonInfoRenShu;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
};
