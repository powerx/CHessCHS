
// ChessCHSDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ChessCHS.h"
#include "ChessCHSDlg.h"
#include "DlgProxy.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "ConnectDlg.h"

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
	EnableActiveAccessibility();
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CChessCHSDlg 对话框



IMPLEMENT_DYNAMIC(CChessCHSDlg, CDialogEx);

CChessCHSDlg::CChessCHSDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CChessCHSDlg::IDD, pParent)
	, m_nCurRecordPos(-1)
	, m_bGameConnect(false)
	, m_CRectCEditText(799, 697, 966, 718)
	, m_CRectCDialogFrame(0, 0, 1024, 738)
	, m_CRectChessTileRed(140, 470, 196, 526)
	, m_CRectChessTileBlack(140, 270, 196, 326)
	, m_CRectChessPosition(220, 50, 770, 662)
	, m_CRectFaceImageMySelf(46, 123, 78, 155)
	, m_CRectCListCtrlInfo(797, 142, 1008, 389)
	, m_CRectFaceImageOpponent(46, 553, 78, 585)
	, m_CRectCRichEditCtrlMsg(797, 403, 1008, 693)
	, m_CUDPSocketCHSMultiCast(Wzz::CUDPSocketCHS::UDPSOCKETTYPE_MULTICAST)
	, m_CUDPSocketCHSBroadCast(Wzz::CUDPSocketCHS::UDPSOCKETTYPE_BROADCAST)
	, m_CMyButtonMin(Wzz::CButtonCHS::s_MyButtonInfoMin)
	, m_CMyButtonExit(Wzz::CButtonCHS::s_MyButtonInfoExit)
	, m_CMyButtonTool(Wzz::CButtonCHS::s_MyButtonInfoTool)
	, m_CMyButtonSend(Wzz::CButtonCHS::s_MyButtonInfoSend)
	, m_CMyButtonDaPu(Wzz::CButtonCHS::s_MyButtonInfoDaPu)
	, m_CMyButtonClose(Wzz::CButtonCHS::s_MyButtonInfoClose)
	, m_CMyButtonHuiQi(Wzz::CButtonCHS::s_MyButtonInfoHuiQi)
	, m_CMyButtonQiuHe(Wzz::CButtonCHS::s_MyButtonInfoQiuHe)
	, m_CMyButtonStart(Wzz::CButtonCHS::s_MyButtonInfoStart)
	, m_CMyButtonRenShu(Wzz::CButtonCHS::s_MyButtonInfoRenShu)
	, m_CMyButtonDaTing(Wzz::CButtonCHS::s_MyButtonInfoDaTing)
	, m_CMyButtonConfig(Wzz::CButtonCHS::s_MyButtonInfoConfig)
	, m_CMyButtonInvateFriend(Wzz::CButtonCHS::s_MyButtonInfoInvatefriend)
{
	EnableActiveAccessibility();
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pAutoProxy = NULL;
}

CChessCHSDlg::~CChessCHSDlg()
{
	// 如果该对话框有自动化代理，则
	//  将此代理指向该对话框的后向指针设置为 NULL，以便
	//  此代理知道该对话框已被删除。
	if (m_pAutoProxy != NULL)
		m_pAutoProxy->m_pDialog = NULL;
}

void CChessCHSDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_CLOSE, m_CMyButtonClose);
	DDX_Control(pDX, IDC_BUTTON_MIN, m_CMyButtonMin);
	DDX_Control(pDX, IDC_BUTTON_INVATEFRIEND, m_CMyButtonInvateFriend);
	DDX_Control(pDX, IDC_BUTTON_CONFIG, m_CMyButtonConfig);
	DDX_Control(pDX, IDC_BUTTON_DATING, m_CMyButtonDaTing);
	DDX_Control(pDX, IDC_BUTTON_EXIT, m_CMyButtonExit);
	DDX_Control(pDX, IDC_BUTTON_TOOL, m_CMyButtonTool);
	DDX_Control(pDX, IDC_BUTTON_SEND, m_CMyButtonSend);
	DDX_Control(pDX, IDC_BUTTON_START, m_CMyButtonStart);
	DDX_Control(pDX, IDC_BUTTON_DAPU, m_CMyButtonDaPu);
	DDX_Control(pDX, IDC_BUTTON_HUIQI, m_CMyButtonHuiQi);
	DDX_Control(pDX, IDC_BUTTON_RENSHU, m_CMyButtonRenShu);
	DDX_Control(pDX, IDC_BUTTON_QUIHE, m_CMyButtonQiuHe);
	DDX_Control(pDX, IDC_EDIT_TEXT, m_CEditText);
	DDX_Control(pDX, IDC_LIST_USER, m_CListCtrlUser);
	DDX_Control(pDX, IDC_RICHEDIT2_MSG, m_CRichEditCtrlMsg);
	DDX_Control(pDX, IDC_LIST_CHESS, m_CListCtrlChess);
}

BEGIN_MESSAGE_MAP(CChessCHSDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_CLOSE()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_ERASEBKGND()
	ON_WM_TIMER()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON_INVATEFRIEND, &CChessCHSDlg::OnBnClickedButtonInvatefriend)
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CChessCHSDlg::OnBnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON_MIN, &CChessCHSDlg::OnBnClickedButtonMin)
	ON_WM_LBUTTONDOWN()
	ON_BN_CLICKED(IDC_BUTTON_START, &CChessCHSDlg::OnBnClickedButtonStart)
	ON_BN_CLICKED(IDC_BUTTON_RENSHU, &CChessCHSDlg::OnBnClickedButtonRenShu)
	ON_BN_CLICKED(IDC_BUTTON_EXIT, &CChessCHSDlg::OnBnClickedButtonExit)
	ON_BN_CLICKED(IDC_BUTTON_SEND, &CChessCHSDlg::OnBnClickedButtonSend)
	ON_EN_CHANGE(IDC_EDIT_TEXT, &CChessCHSDlg::OnEnChangeEditText)
	ON_BN_CLICKED(IDC_BUTTON_QUIHE, &CChessCHSDlg::OnBnClickedButtonQuihe)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_USER, &CChessCHSDlg::OnNMRClickListUser)
	ON_COMMAND(IDM_USERLIST_CONNECT, &CChessCHSDlg::OnUserListConnect)
	ON_COMMAND(IDM_USERLIST_SHUAXIN, &CChessCHSDlg::OnUserListShuaXin)
	ON_COMMAND(IDM_OPTION_CONNECT, &CChessCHSDlg::OnOptionConnect)
	ON_COMMAND(IDM_CHESSLIST_SHOW, &CChessCHSDlg::OnChessListShow)
	ON_COMMAND(IDM_USERLIST_SHOW, &CChessCHSDlg::OnUserListShow)
	ON_NOTIFY(NM_RCLICK, IDC_LIST_CHESS, &CChessCHSDlg::OnNMRClickListChess)
	ON_COMMAND(IDM_CHATMSG_CLEAR, &CChessCHSDlg::OnChatMsgClear)
END_MESSAGE_MAP()


// CChessCHSDlg 消息处理程序

BOOL CChessCHSDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	HideCaption();
	CenterWindow();
	InitCDialogFrame();
	InitCEditText();
	InitCListCtrlInfo();
	InitCRichEditCtrlMsg();
	InitFaceImage();

	InitCTCPSocketCH();
	
	if (InitCUDPSocketCHBroadCast() &&	InitCUDPSocketCHMultiCast())
	{
		Wzz::CUDPSocketCHS::UdpMessagePkt udpMsg;
		memset(&udpMsg, 0, sizeof(Wzz::CUDPSocketCHS::UdpMessagePkt));
		udpMsg.msgType = Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_LINEON;
		udpMsg.guid = Wzz::CUDPSocketCHS::CreateNewGuid();
		CString csSend(L"我来也！");
		memmove_s(udpMsg.szMsg, sizeof(udpMsg.szMsg), CW2A(csSend), csSend.GetLength());

		m_CUDPSocketCHSBroadCast.SendTo(udpMsg);
		m_CUDPSocketCHSMultiCast.SendTo(udpMsg);
	}

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CChessCHSDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CChessCHSDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CChessCHSDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// 当用户关闭 UI 时，如果控制器仍保持着它的某个
//  对象，则自动化服务器不应退出。这些
//  消息处理程序确保如下情形: 如果代理仍在使用，
//  则将隐藏 UI；但是在关闭对话框时，
//  对话框仍然会保留在那里。

void CChessCHSDlg::OnClose()
{
	if (CanExit())
	{
		if (m_bGameConnect)
		{
			if (IDOK == MessageBox(L"您确定要退出吗？", L"提示", MB_OKCANCEL))
			{
				Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
				memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
				tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEEXIT_MESSAGE;
				m_CTCPSocketCHSClient.Send(tcpMessagePktSend);

				Wzz::CUDPSocketCHS::UdpMessagePkt udpMsg;
				memset(&udpMsg, 0, sizeof(Wzz::CUDPSocketCHS::UdpMessagePkt));
				udpMsg.msgType = Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_LINEOUT;
				udpMsg.guid = Wzz::CUDPSocketCHS::CreateNewGuid();
				CString csSend(L"我走了！");
				memmove_s(udpMsg.szMsg, sizeof(udpMsg.szMsg), CW2A(csSend), csSend.GetLength());

				m_CUDPSocketCHSMultiCast.SendTo(udpMsg);
				m_CUDPSocketCHSBroadCast.SendTo(udpMsg);
				m_CTCPSocketCHSServer.Close();
				m_CTCPSocketCHSClient.Close();
				m_CUDPSocketCHSMultiCast.Close();
				m_CUDPSocketCHSBroadCast.Close();
				CDialogEx::OnClose();
			}
		}
		else
		{
			Wzz::CUDPSocketCHS::UdpMessagePkt udpMsg;
			memset(&udpMsg, 0, sizeof(Wzz::CUDPSocketCHS::UdpMessagePkt));
			udpMsg.msgType = Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_LINEOUT;
			udpMsg.guid = Wzz::CUDPSocketCHS::CreateNewGuid();
			CString csSend(L"我走了！");
			memmove_s(udpMsg.szMsg, sizeof(udpMsg.szMsg), CW2A(csSend), csSend.GetLength());

			m_CUDPSocketCHSMultiCast.SendTo(udpMsg);
			m_CUDPSocketCHSBroadCast.SendTo(udpMsg);
			m_CTCPSocketCHSServer.Close();
			m_CTCPSocketCHSClient.Close();
			m_CUDPSocketCHSMultiCast.Close();
			m_CUDPSocketCHSBroadCast.Close();
			CDialogEx::OnClose();
		}
	}
}

void CChessCHSDlg::OnOK()
{
	if (CanExit())
		CDialogEx::OnOK();
}

void CChessCHSDlg::OnCancel()
{
	if (CanExit())
		CDialogEx::OnCancel();
}

BOOL CChessCHSDlg::CanExit()
{
	// 如果代理对象仍保留在那里，则自动化
	//  控制器仍会保持此应用程序。
	//  使对话框保留在那里，但将其 UI 隐藏起来。
	if (m_pAutoProxy != NULL)
	{
		ShowWindow(SW_HIDE);
		return FALSE;
	}

	return TRUE;
}


void CChessCHSDlg::ShowBackGround(CDC* pDC)
{
	ASSERT(NULL != pDC);

	CBitmap bitmap;
	BITMAP bit;
	CDC dcCompatible;
	dcCompatible.CreateCompatibleDC(pDC);// 创建一个兼容DC

	bitmap.LoadBitmap(MAKEINTRESOURCE(IDB_MAIN_BACKGROUND));

	bitmap.GetBitmap(&bit);
	CBitmap* pOldBitmap = dcCompatible.SelectObject(&bitmap);

	pDC->BitBlt(0, 0
		, bit.bmWidth, bit.bmHeight, &dcCompatible
		, 0, 0, SRCCOPY);

	dcCompatible.SelectObject(pOldBitmap);
	bitmap.DeleteObject();
}

void CChessCHSDlg::InitCButtonStatus()
{
	m_CMyButtonStart.EnableWindow(TRUE);
	m_CMyButtonQiuHe.EnableWindow(FALSE);
	m_CMyButtonRenShu.EnableWindow(FALSE);
	m_CMyButtonHuiQi.EnableWindow(FALSE);
	m_CMyButtonDaPu.EnableWindow(FALSE);
	m_CChessInfoCHS.SetGameStart(false);

}

void CChessCHSDlg::InitCDialogFrame()
{
	SetWindowPos(&wndTop, 0, 0, m_CRectCDialogFrame.Width(), m_CRectCDialogFrame.Height(), SWP_FRAMECHANGED);
}


void CChessCHSDlg::InitCEditText()
{
	m_CEditText.MoveWindow(m_CRectCEditText.left
		, m_CRectCEditText.top
		, m_CRectCEditText.right-m_CRectCEditText.left
		, m_CRectCEditText.bottom-m_CRectCEditText.top);
}

void CChessCHSDlg::InitCRichEditCtrlMsg()
{
	// 以下为设置控件内的字体样式
	CHARFORMAT cf = {0};
	m_CRichEditCtrlMsg.GetDefaultCharFormat(cf);
	cf.cbSize = sizeof(cf);
	lstrcpy(cf.szFaceName, L"Impact");
	cf.yHeight = 200;
	cf.dwEffects &= ~CFE_BOLD & ~CFE_AUTOCOLOR; // 取消默认的粗体字、默认颜色
	cf.crTextColor = RGB(100, 200, 150);        // 设置字体的颜色
	cf.dwMask = CFM_ALL;                        // 各项数据均有效
	m_CRichEditCtrlMsg.SetWordCharFormat(cf);

	m_CRichEditCtrlMsg.SetBackgroundColor(FALSE, RGB(70, 71, 50));

	m_CRichEditCtrlMsg.MoveWindow(m_CRectCRichEditCtrlMsg.left
		, m_CRectCRichEditCtrlMsg.top
		, m_CRectCRichEditCtrlMsg.right-m_CRectCRichEditCtrlMsg.left
		, m_CRectCRichEditCtrlMsg.bottom-m_CRectCRichEditCtrlMsg.top);

	m_CRichEditCtrlMsg.SetEventMask(ENM_MOUSEEVENTS);
}

void CChessCHSDlg::InitCListCtrlInfo()
{
	m_CListCtrlUser.MoveWindow(m_CRectCListCtrlInfo.left
		, m_CRectCListCtrlInfo.top
		, m_CRectCListCtrlInfo.right-m_CRectCListCtrlInfo.left
		, m_CRectCListCtrlInfo.bottom-m_CRectCListCtrlInfo.top);

	m_CListCtrlUser.SetBkColor(RGB(70, 71, 50));      // 设置列表控件的背景颜色
	m_CListCtrlUser.SetTextBkColor(RGB(70, 71, 50));  // 设置列表控件的字体背景颜色
	m_CListCtrlUser.SetTextColor(RGB(100, 200, 150));
	// 将列表控件设置为可以选中整行的模式
	m_CListCtrlUser.SetExtendedStyle(m_CListCtrlUser.GetExtendedStyle() | LVS_EX_FULLROWSELECT);
	// 插入列
	LVCOLUMN lvcolumnUser;

	lvcolumnUser.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH;
	lvcolumnUser.fmt = LVCFMT_CENTER;
	lvcolumnUser.cx = 60;
	lvcolumnUser.pszText = L"编号";
	m_CListCtrlUser.InsertColumn(0, &lvcolumnUser);

	lvcolumnUser.cx = 100;
	lvcolumnUser.pszText = L"IP地址";
	m_CListCtrlUser.InsertColumn(1, &lvcolumnUser);

	m_CListCtrlUser.ModifyStyle(NULL, WS_VISIBLE);


	m_CListCtrlChess.MoveWindow(m_CRectCListCtrlInfo.left
		, m_CRectCListCtrlInfo.top
		, m_CRectCListCtrlInfo.right-m_CRectCListCtrlInfo.left
		, m_CRectCListCtrlInfo.bottom-m_CRectCListCtrlInfo.top);

	m_CListCtrlChess.SetBkColor(RGB(70, 71, 50));      // 设置列表控件的背景颜色
	m_CListCtrlChess.SetTextBkColor(RGB(70, 71, 50));  // 设置列表控件的字体背景颜色
	m_CListCtrlChess.SetTextColor(RGB(100, 200, 150));
	// 将列表控件设置为可以选中整行的模式
	m_CListCtrlChess.SetExtendedStyle(m_CListCtrlChess.GetExtendedStyle() | LVS_EX_FULLROWSELECT);
	// 插入列
	LVCOLUMN lvcolumnChess;

	lvcolumnChess.mask = LVCF_FMT | LVCF_SUBITEM | LVCF_TEXT | LVCF_WIDTH;
	lvcolumnChess.fmt = LVCFMT_CENTER;
	lvcolumnChess.cx = 60;
	lvcolumnChess.pszText = L"编号";
	m_CListCtrlChess.InsertColumn(0, &lvcolumnChess);

	lvcolumnChess.cx = 100;
	lvcolumnChess.pszText = L"棋谱";
	m_CListCtrlChess.InsertColumn(1, &lvcolumnChess);

	m_CListCtrlChess.ModifyStyle(WS_VISIBLE, NULL);
}


void CChessCHSDlg::InitFaceImage()
{
	GetDlgItem(IDC_FACE_OPPONENT)->MoveWindow(m_CRectFaceImageOpponent.left
		, m_CRectFaceImageOpponent.top
		, m_CRectFaceImageOpponent.right-m_CRectFaceImageOpponent.left
		, m_CRectFaceImageOpponent.bottom-m_CRectFaceImageOpponent.top);
	GetDlgItem(IDC_FACE_MYSELF)->MoveWindow(m_CRectFaceImageMySelf.left
		, m_CRectFaceImageMySelf.top
		, m_CRectFaceImageMySelf.right-m_CRectFaceImageMySelf.left
		, m_CRectFaceImageMySelf.bottom-m_CRectFaceImageMySelf.top);
}


void CChessCHSDlg::OnBnClickedButtonInvatefriend()
{
	// TODO: 在此添加控件通知处理程序代码
	if (OpenClipboard())
	{
		EmptyClipboard();
		char strMsg[] = "欢迎您使用“中国象棋”（开发者联系方式：630067567@QQ.COM）！";
		HANDLE hClip = GlobalAlloc(GMEM_MOVEABLE, sizeof(strMsg)+1);
		char* pBuf = (char*)GlobalLock(hClip);
		strcpy_s(pBuf, sizeof(strMsg)+1, strMsg);
		GlobalUnlock(hClip);
		SetClipboardData(CF_TEXT, hClip);
		CloseClipboard();
	}
}



BOOL CChessCHSDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	ShowBackGround(pDC);
	m_CChessInfoCHS.DrawAllChess(pDC);

	int nOldBkMode = pDC->SetBkMode(TRANSPARENT);
	pDC->DrawText(m_CsUserNameYou, CRect(82, 121, 150, 138), NULL);
	pDC->DrawText(m_CsPCUserNameYou, CRect(82, 138, 150, 156), NULL);
	pDC->SetBkMode(nOldBkMode);

	return FALSE; // return CDialogEx::OnEraseBkgnd(pDC);
}

void CChessCHSDlg::HideCaption() const
{
	LONG lStyle = ::GetWindowLongW(this->m_hWnd, GWL_STYLE);
	SetWindowLongW(this->m_hWnd, GWL_STYLE, lStyle&~WS_CAPTION);
	::SetWindowPos(this->m_hWnd, NULL, 0, 0, 0, 0,
		SWP_NOSIZE | SWP_NOMOVE | SWP_NOZORDER/* | SWP_NOACTIVATE | SWP_FRAMECHANGED*/);
}


void CChessCHSDlg::OnBnClickedButtonClose()
{
	// TODO: 在此添加控件通知处理程序代码
	SendMessage(WM_CLOSE);
}


void CChessCHSDlg::OnBnClickedButtonMin()
{
	// TODO: 在此添加控件通知处理程序代码
	ShowWindow(SW_MINIMIZE);
}


void CChessCHSDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (!((point.x>=220 && point.y>=50) && (point.x<=770 && point.y<=662)))
	{
		SendMessage(WM_SYSCOMMAND, SC_MOVE | HTCAPTION, 0);
	}
	else
	{
		if (m_CChessInfoCHS.GetMySelfRun())
		{
			if (m_CChessInfoCHS.ChessClicked(point))
			{
				Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePkt;
				memset(&tcpMessagePkt, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));

				bool bKing = m_CChessInfoCHS.IsChessKing(m_CChessInfoCHS.GetCPointChessCHS(point));
				if (bKing)
				{
					tcpMessagePkt.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEWIN_REQUEST;
				}
				else
				{
					tcpMessagePkt.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEPLAY_REQUEST;
				}

				tcpMessagePkt.pointStart = m_CChessInfoCHS.GetChessFocusStart();
				tcpMessagePkt.pointStop = m_CChessInfoCHS.GetChessFocusStop();
				
				m_CTCPSocketCHSClient.Send(tcpMessagePkt);

				if (bKing)
				{
					MessageBox(L"恭喜您，您赢了！", L"提示");
				}
			}
		}
	}

	CDialogEx::OnLButtonDown(nFlags, point);
}


void CChessCHSDlg::OnBnClickedButtonStart()
{
	// TODO: 在此添加控件通知处理程序代码
	Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePkt;
	memset(&tcpMessagePkt, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
	tcpMessagePkt.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMESTART_REQUEST;
	m_CTCPSocketCHSClient.Send(tcpMessagePkt);
}


void CChessCHSDlg::OnBnClickedButtonRenShu()
{
	// TODO: 在此添加控件通知处理程序代码
	if (IDOK == MessageBox(L"您确定要认输吗？", L"提示", MB_OKCANCEL))
	{
		Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
		memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
		tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMERENSHU_REQUEST;
		m_CTCPSocketCHSClient.Send(tcpMessagePktSend);
	}
}


void CChessCHSDlg::OnBnClickedButtonExit()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_CChessInfoCHS.GetGameStart())
	{
		if (IDOK == MessageBox(L"您确定要退出本局吗？", L"提示", MB_OKCANCEL))
		{
			Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
			memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
			tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEEXIT_MESSAGE;
			m_CTCPSocketCHSClient.Send(tcpMessagePktSend);

			InitCButtonStatus();

			InvalidateRect(m_CRectChessTileRed, TRUE);
			InvalidateRect(m_CRectChessTileBlack, TRUE);
			InvalidateRect(m_CRectChessPosition, TRUE);

			InitCTCPSocketCH();
		}
	}
}



void CChessCHSDlg::OnEnChangeEditText()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	CString csText;
	m_CEditText.GetWindowText(csText);
	if (50 < csText.GetLength())
	{
		m_CEditText.SetWindowText(csText.Left(50));
		m_CEditText.SetSel(50, 50);
	}
}


void CChessCHSDlg::OnBnClickedButtonSend()
{
	// TODO: 在此添加控件通知处理程序代码
	CString cs;
	m_CEditText.GetWindowText(cs);       // 获取CEdit编辑框中的数据
	m_CEditText.SetWindowText(L"");      // 清除编辑框中的内容
	if (!cs.IsEmpty())
	{
		CString csSend(cs+L"\n");
		Wzz::CUDPSocketCHS::UdpMessagePkt udpMsg;
		memset(&udpMsg, 0, sizeof(Wzz::CUDPSocketCHS::UdpMessagePkt));
		udpMsg.msgType = Wzz::CUDPSocketCHS::UDPMESSAGETYPE_CHATMSG;
		udpMsg.guid = Wzz::CUDPSocketCHS::CreateNewGuid();
		memmove_s(udpMsg.szMsg, sizeof(udpMsg.szMsg), CW2A(csSend), csSend.GetLength());

		m_CUDPSocketCHSBroadCast.SendTo(udpMsg);

		CString csMsg(CString(L"我说：")+cs+L"\n");
		// 将自己发送的消息显示在富编辑框中
		m_CRichEditCtrlMsg.SetSel(-1, -1);
		m_CRichEditCtrlMsg.ReplaceSel(csMsg);
	}
}



void CChessCHSDlg::OnUDPReceive(const Wzz::CUDPSocketCHS::UdpSocketType type, int nErrorCode)
{
	OutputDebugString(L"OnUDPReceive");

	char szRecvBuffer[1600] = {'\0'};
	SOCKADDR_IN addr = {0};
	int nSockAddrLen = sizeof(SOCKADDR);
	int nRecvLen = 0;
	switch (type)
	{
	case Wzz::CUDPSocketCHS::UDPSOCKETTYPE_BROADCAST:
		nRecvLen = m_CUDPSocketCHSBroadCast.ReceiveFrom(szRecvBuffer, sizeof(szRecvBuffer), (SOCKADDR*)&addr, &nSockAddrLen);
		break;
	case Wzz::CUDPSocketCHS::UDPSOCKETTYPE_MULTICAST:
		nRecvLen = m_CUDPSocketCHSMultiCast.ReceiveFrom(szRecvBuffer, sizeof(szRecvBuffer), (SOCKADDR*)&addr, &nSockAddrLen);
		break;
	default:
		return;  // 退出
		break;
	}
	IN_ADDR inAddr = {0};
	inAddr.S_un.S_addr = addr.sin_addr.S_un.S_addr;

	Wzz::CUDPSocketCHS::UdpMessagePkt udpMsg;
	memset(&udpMsg, 0, sizeof(Wzz::CUDPSocketCHS::UdpMessagePkt));
	if (Wzz::CUDPSocketCHS::ParseUdpMessagePkt(udpMsg, szRecvBuffer, nRecvLen))
	{
		if (!Wzz::CUDPSocketCHS::ExistsRecvGuidList(udpMsg.guid))
		{
			switch (udpMsg.msgType)
			{
			case Wzz::CUDPSocketCHS::UDPMESSAGETYPE_CHATMSG:
				{
					CString csRecv(CString(CA2W(inet_ntoa(inAddr)))+CString(L"说：")+CString(CA2W(udpMsg.szMsg))+CString(L"\n"));
					m_CRichEditCtrlMsg.SetSel(-1, -1);
					m_CRichEditCtrlMsg.ReplaceSel(csRecv);

					UpDateUserList(inAddr, true);

					break;
				}
			case Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_LINEON:
				{
					CString csRecv(CString(L"（上线提醒）")+CString(CA2W(inet_ntoa(inAddr)))+CString(L"\n"));
					m_CRichEditCtrlMsg.SetSel(-1, -1);
					m_CRichEditCtrlMsg.ReplaceSel(csRecv);

					UpDateUserList(inAddr, true);

					Wzz::CUDPSocketCHS::UdpMessagePkt udpMsg;
					memset(&udpMsg, 0, sizeof(Wzz::CUDPSocketCHS::UdpMessagePkt));
					udpMsg.msgType = Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_RESPONSE;
					udpMsg.guid = Wzz::CUDPSocketCHS::CreateNewGuid();
					CString csSend(L"我来也！");
					memmove_s(udpMsg.szMsg, sizeof(udpMsg.szMsg), CW2A(csSend), csSend.GetLength());

					m_CUDPSocketCHSBroadCast.SendTo(udpMsg);

					break;
				}
			case Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_LINEOUT:
				{
					CString csRecv(CString(L"（下线提醒）")+CString(CA2W(inet_ntoa(inAddr)))+CString(L"\n"));
					m_CRichEditCtrlMsg.SetSel(-1, -1);
					m_CRichEditCtrlMsg.ReplaceSel(csRecv);

					UpDateUserList(inAddr, false);

					break;
				}
			case Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_REQUEST:
				{
					Wzz::CUDPSocketCHS::UdpMessagePkt udpMsg;
					memset(&udpMsg, 0, sizeof(Wzz::CUDPSocketCHS::UdpMessagePkt));
					udpMsg.msgType = Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_RESPONSE;
					udpMsg.guid = Wzz::CUDPSocketCHS::CreateNewGuid();

					m_CUDPSocketCHSBroadCast.SendTo(udpMsg);

					// break;  // 继续进行，刷新列表，如果没有就继续添加
				}
			case Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_RESPONSE:
				{
					UpDateUserList(inAddr, true);

					break;
				}
			default:
				break;
			}
		}
	}
}


void CChessCHSDlg::OnTCPReceive(int nErrorCode)
{
	if (0 == nErrorCode)
	{
		char szRecvBuffer[1024] = {'\0'};

		int nRecvLen = m_CTCPSocketCHSClient.Receive(szRecvBuffer, sizeof(szRecvBuffer));
		Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktRecv;
		memset(&tcpMessagePktRecv, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));

		if (Wzz::CTCPSocketCHS::ParseTcpMessagePkt(tcpMessagePktRecv, szRecvBuffer, nRecvLen))
		{
			switch (tcpMessagePktRecv.msgType)
			{
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_USERINFO_REQUEST:
				{
					Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
					memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
					tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_USERINFO_RESPONSE;
					strcpy_s(tcpMessagePktSend.szUserName, "HongXG");
					strcpy_s(tcpMessagePktSend.szPCUserName, "Admin");
					m_CTCPSocketCHSClient.Send(tcpMessagePktSend);

					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_USERINFO_RESPONSE:
				{
					m_CsUserNameYou = CString(tcpMessagePktRecv.szUserName);
					m_CsPCUserNameYou = CString(tcpMessagePktRecv.szPCUserName);

					InvalidateRect(CRect(82, 121, 150, 138));
					InvalidateRect(CRect(82, 138, 150, 156));

					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEWIN_REQUEST:
				{
					Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
					memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
					tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEWIN_RESPONSE;
					m_CTCPSocketCHSClient.Send(tcpMessagePktSend);

					m_CChessInfoCHS.SetMySelfRun(false);
					m_CChessInfoCHS.SetGameStart(false);

					CPoint pointTransStart = Wzz::CChessInfoCHS::TranslateCPoint(tcpMessagePktRecv.pointStart);
					CPoint pointTransStop = Wzz::CChessInfoCHS::TranslateCPoint(tcpMessagePktRecv.pointStop);
					m_CChessInfoCHS.ChessMoveOrEat(pointTransStart, pointTransStop);

					MessageBox(L"很遗憾，您输了！", L"提示");

					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEPLAY_REQUEST:
				{
					Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
					memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
					tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEPLAY_RESPONSE;
					m_CTCPSocketCHSClient.Send(tcpMessagePktSend);

					m_CChessInfoCHS.SetMySelfRun(true);

					CPoint pointTransStart = Wzz::CChessInfoCHS::TranslateCPoint(tcpMessagePktRecv.pointStart);
					CPoint pointTransStop = Wzz::CChessInfoCHS::TranslateCPoint(tcpMessagePktRecv.pointStop);
					m_CChessInfoCHS.ChessMoveOrEat(pointTransStart, pointTransStop);

					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEQIUHE_REQUEST:
				{
					Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
					memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
					if (IDOK == MessageBox(L"对方请求和棋，您同意吗？", L"提示", MB_OKCANCEL))
					{
						tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEQIUHE_AGREE;
					}
					else
					{
						tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEQIUHE_REFUSE;
					}
					m_CTCPSocketCHSClient.Send(tcpMessagePktSend);
					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEQIUHE_AGREE:
				{
					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEQIUHE_REFUSE:
				{
					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMESTART_REQUEST:
				{
					Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
					memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
					if (IDOK == MessageBox(L"对方请求开始游戏，您同意吗？", L"提示", MB_OKCANCEL))
					{
						tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMESTART_AGREE;
						m_CMyButtonStart.EnableWindow(FALSE);
						m_CMyButtonQiuHe.EnableWindow(TRUE);
						m_CMyButtonRenShu.EnableWindow(TRUE);
						m_CMyButtonHuiQi.EnableWindow(TRUE);
						m_CMyButtonDaPu.EnableWindow(TRUE);

						m_CChessInfoCHS.SetMySelfRun(false);
						m_CChessInfoCHS.SetGameStart(true);
						m_CChessInfoCHS.InitChessInfo();
						InvalidateRect(m_CRectChessTileRed);
						InvalidateRect(m_CRectChessTileBlack);
						InvalidateRect(m_CRectChessPosition);
					}
					else
					{
						tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMESTART_REFUSE;
					}
					m_CTCPSocketCHSClient.Send(tcpMessagePktSend);
					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMESTART_AGREE:
				{
					m_CMyButtonStart.EnableWindow(FALSE);
					m_CMyButtonQiuHe.EnableWindow(TRUE);
					m_CMyButtonRenShu.EnableWindow(TRUE);
					m_CMyButtonHuiQi.EnableWindow(TRUE);
					m_CMyButtonDaPu.EnableWindow(TRUE);

					m_CChessInfoCHS.SetMySelfRun(true);
					m_CChessInfoCHS.SetGameStart(true);
					m_CChessInfoCHS.InitChessInfo();
					InvalidateRect(m_CRectChessTileRed);
					InvalidateRect(m_CRectChessTileBlack);
					InvalidateRect(m_CRectChessPosition);
					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMESTART_REFUSE:
				{
					m_CChessInfoCHS.SetMySelfRun(false);
					m_CChessInfoCHS.SetGameStart(false);
					MessageBox(L"对方拒绝开始游戏！", L"提示");
					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMERENSHU_REQUEST:
				{
					Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
					memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
					tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMERENSHU_RESPONSE;
					m_CTCPSocketCHSClient.Send(tcpMessagePktSend);

					InitCButtonStatus();

					m_CChessInfoCHS.SetGameStart(false);

					InvalidateRect(m_CRectChessTileRed, TRUE);
					InvalidateRect(m_CRectChessTileBlack, TRUE);

					MessageBox(L"游戏结束，对方认输，您赢了！", L"提示");
					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMERENSHU_RESPONSE:
				{
					InitCButtonStatus();

					m_CChessInfoCHS.SetGameStart(false);

					InvalidateRect(m_CRectChessTileRed, TRUE);
					InvalidateRect(m_CRectChessTileBlack, TRUE);
					InvalidateRect(m_CRectChessPosition, TRUE);

					MessageBox(L"游戏结束，您输了！", L"提示");
					break;
				}
			case Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEEXIT_MESSAGE:
				{
					InitCTCPSocketCH();

					InitCButtonStatus();

					m_CChessInfoCHS.SetGameStart(false);

					m_CsUserNameYou = CString();
					m_CsPCUserNameYou = CString();

					InvalidateRect(CRect(82, 121, 150, 138));
					InvalidateRect(CRect(82, 138, 150, 156));

					InvalidateRect(m_CRectChessTileRed, TRUE);
					InvalidateRect(m_CRectChessTileBlack, TRUE);
					InvalidateRect(m_CRectChessPosition, TRUE);

					MessageBox(L"对方已经退出，游戏结束！", L"提示");
					
					break;
				}
			default:
				break;
			}
		}
	}
}


void CChessCHSDlg::OnTCPAccept(int nErrorCode)
{
	if (m_CTCPSocketCHSServer.Accept(m_CTCPSocketCHSClient))
	{
		m_CTCPSocketCHSServer.Close();
		m_bGameConnect = true;

		Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePkt;
		memset(&tcpMessagePkt, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
		tcpMessagePkt.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_USERINFO_REQUEST;
		strcpy_s(tcpMessagePkt.szUserName, "HongXG");
		strcpy_s(tcpMessagePkt.szPCUserName, "Admin");
		m_CTCPSocketCHSClient.Send(tcpMessagePkt);

		MessageBox(L"连接建立成功，您可点击\"开始\"按钮进行游戏！", L"提示");
	}
}

void CChessCHSDlg::OnTCPConnect(int nErrorCode)
{
	if (0 == nErrorCode)
	{
		m_bGameConnect = true;

		Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePkt;
		memset(&tcpMessagePkt, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
		tcpMessagePkt.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_USERINFO_REQUEST;
		strcpy_s(tcpMessagePkt.szUserName, "HongXG");
		strcpy_s(tcpMessagePkt.szPCUserName, "Admin");
		m_CTCPSocketCHSClient.Send(tcpMessagePkt);

		MessageBox(L"连接建立成功，您可点击\"开始\"按钮进行游戏！", L"提示");
	}
	else
	{
		MessageBox(L"连接失败！", L"提示");
	}
}

void CChessCHSDlg::OnBnClickedButtonQuihe()
{
	// TODO: 在此添加控件通知处理程序代码
	Wzz::CTCPSocketCHS::TcpMessagePkt tcpMessagePktSend;
	memset(&tcpMessagePktSend, 0, sizeof(Wzz::CTCPSocketCHS::TcpMessagePkt));
	tcpMessagePktSend.msgType = Wzz::CTCPSocketCHS::TCPMESSAGETYPE_GAMEQIUHE_REQUEST;
	m_CTCPSocketCHSClient.Send(tcpMessagePktSend);

	MessageBox(L"游戏结束，您输了！", L"提示");
}

bool CChessCHSDlg::InitCUDPSocketCHMultiCast(void)
{
	if (m_CUDPSocketCHSMultiCast.Create(MULTICAST_PORT, SOCK_DGRAM))
	{
		int nSockReuse = 1;
		if (m_CUDPSocketCHSMultiCast.SetSockOpt(SO_REUSEADDR, &nSockReuse, sizeof(nSockReuse), SOL_SOCKET))
		{
			struct ip_mreq multicast;
			multicast.imr_multiaddr.s_addr = inet_addr(MULTICAST_IPADDR);   
			multicast.imr_interface.s_addr = htonl(INADDR_ANY);   

			if (m_CUDPSocketCHSMultiCast.SetSockOpt(IP_ADD_MEMBERSHIP, &multicast, sizeof(multicast), IPPROTO_IP))
			{
				int nTtl = 255;
				if (m_CUDPSocketCHSMultiCast.SetSockOpt(IP_MULTICAST_TTL, &nTtl, sizeof(nTtl), IPPROTO_IP))
				{
					return true;
				}
			}   
		}   
	}
	m_CUDPSocketCHSMultiCast.Close();
	return false;
}

bool CChessCHSDlg::InitCUDPSocketCHBroadCast(void)
{
	if (m_CUDPSocketCHSBroadCast.Create(BROADCAST_PORT, SOCK_DGRAM))
	{
		BOOL bOptValue = TRUE;
		if (m_CUDPSocketCHSBroadCast.SetSockOpt(SO_BROADCAST, &bOptValue, sizeof(bOptValue), SOL_SOCKET))
		{
			return true;
		}
	}
	m_CUDPSocketCHSBroadCast.Close();
	return false;
}


bool CChessCHSDlg::InitCTCPSocketCH(void)
{
	m_CTCPSocketCHSServer.Close();
	m_CTCPSocketCHSClient.Close();
	if (m_CTCPSocketCHSServer.Create(8812))
	{
		DWORD dwArgument = 1;
		m_CTCPSocketCHSServer.IOCtl(FIONBIO, &dwArgument);
		int nTimeOutSend = 500;
		m_CTCPSocketCHSServer.SetSockOpt(SO_SNDTIMEO, (char*)&nTimeOutSend, sizeof(nTimeOutSend), SOL_SOCKET);

		m_CTCPSocketCHSServer.Listen(1);
	}
	else
	{
		MessageBox(L"TCP网络套接字初始化失败！", L"提示");
	}

	return false;
}


void CChessCHSDlg::OnNMRClickListUser(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	m_nCurRecordPos=pNMItemActivate->iItem; // 获取当前点击右键时，光标所处在的有效列表位置
	if (m_nCurRecordPos < m_CListCtrlUser.GetItemCount()
		&& m_nCurRecordPos >= 0)
	{
		CMenu menu_main;
		menu_main.LoadMenu(IDR_MENU_POPUPUSERLIST);
		CMenu* menuSon = menu_main.GetSubMenu(0);

		CPoint point;
		GetCursorPos(&point);
		menuSon->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	}
	else
	{
		CMenu menu_main;
		menu_main.LoadMenu(IDR_MENU_POPUPUSERLIST);
		CMenu* menuSon = menu_main.GetSubMenu(0);
		menuSon->DeleteMenu(0, MF_BYPOSITION);
		CPoint point;
		GetCursorPos(&point);
		menuSon->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	}

	*pResult = 0;
}

void CChessCHSDlg::OnNMRClickListChess(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	m_nCurRecordPos=pNMItemActivate->iItem; // 获取当前点击右键时，光标所处在的有效列表位置
	if (m_nCurRecordPos < m_CListCtrlUser.GetItemCount()
		&& m_nCurRecordPos >= 0)
	{
		CMenu menu_main;
		menu_main.LoadMenu(IDR_MENU_POPUPUSERLIST);
		CMenu* menuSon = menu_main.GetSubMenu(1);

		CPoint point;
		GetCursorPos(&point);
		menuSon->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	}
	else
	{
		CMenu menu_main;
		menu_main.LoadMenu(IDR_MENU_POPUPUSERLIST);
		CMenu* menuSon = menu_main.GetSubMenu(1);
		//menuSon->DeleteMenu(0, MF_BYPOSITION);
		CPoint point;
		GetCursorPos(&point);
		menuSon->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this);
	}

	*pResult = 0;
}


void CChessCHSDlg::OnUserListConnect()
{
	// TODO: 在此添加命令处理程序代码
	CString csIP = m_CListCtrlUser.GetItemText(m_nCurRecordPos, 1);
	if (csIP.IsEmpty())
	{
		MessageBox(L"列表中IP地址信息有误！", L"提示");
	}
	else
	{
		m_CTCPSocketCHSServer.Close();
		m_CTCPSocketCHSClient.Close();
		m_CTCPSocketCHSClient.Create();
		DWORD dwArgument = 1;
		m_CTCPSocketCHSClient.IOCtl(FIONBIO, &dwArgument);
		int nTimeOutSend = 200;
		m_CTCPSocketCHSClient.SetSockOpt(SO_SNDTIMEO, (char*)&nTimeOutSend, sizeof(nTimeOutSend), SOL_SOCKET);

		m_CTCPSocketCHSClient.Connect(csIP, 8812);
	}
}

void CChessCHSDlg::OnOptionConnect()
{
	// TODO: 在此添加命令处理程序代码
	CConnectDlg dlg;
	if (IDOK == dlg.DoModal())
	{
	}
}


void CChessCHSDlg::OnUserListShuaXin()
{
	// TODO: 在此添加命令处理程序代码
	m_CListCtrlUser.DeleteAllItems();

	Wzz::CUDPSocketCHS::UdpMessagePkt udpMsg;
	memset(&udpMsg, 0, sizeof(Wzz::CUDPSocketCHS::UdpMessagePkt));
	udpMsg.msgType = Wzz::CUDPSocketCHS::UDPMESSAGETYPE_USERLIST_REQUEST;
	udpMsg.guid = Wzz::CUDPSocketCHS::CreateNewGuid();
	CString csSend(L"我来也！");
	memmove_s(udpMsg.szMsg, sizeof(udpMsg.szMsg), CW2A(csSend), csSend.GetLength());

	m_CUDPSocketCHSBroadCast.SendTo(udpMsg);
}



// 更新用户列表
void CChessCHSDlg::UpDateUserList(const IN_ADDR inAddr, const bool bLineOn)
{
	LVFINDINFO lvFindInfo;
	lvFindInfo.flags = LVFI_PARAM;
	lvFindInfo.lParam = (LPARAM)(inAddr.S_un.S_addr);
	int nItem = m_CListCtrlUser.FindItem(&lvFindInfo);

	if (-1 == nItem)
	{
		if (bLineOn)
		{
			int nItemNum = m_CListCtrlUser.GetItemCount();
			LVITEM lvItem;
			lvItem.mask = LVIF_PARAM;
			lvItem.iSubItem = 0;
			lvItem.iItem = nItemNum;
			lvItem.lParam = (LPARAM)(inAddr.S_un.S_addr);
			m_CListCtrlUser.InsertItem(&lvItem);
			CString csNum;
			csNum.Format(L"%d", nItemNum+1);
			m_CListCtrlUser.SetItemText(nItemNum, 0, csNum);
			m_CListCtrlUser.SetItemText(nItemNum, 1, CString(CA2W(inet_ntoa(inAddr))));
		}
	}
	else
	{
		if (bLineOn)
		{
			CString csNum;
			csNum.Format(L"%d", nItem+1);
			m_CListCtrlUser.SetItemText(nItem, 0, csNum);
			m_CListCtrlUser.SetItemText(nItem, 1, CString(CA2W(inet_ntoa(inAddr))));
		}
		else
		{
			m_CListCtrlUser.DeleteItem(nItem);
		}
	}
}


void CChessCHSDlg::OnChessListShow()
{
	// TODO: 在此添加命令处理程序代码
	m_CListCtrlUser.ModifyStyle(WS_VISIBLE, NULL);
	m_CListCtrlChess.ModifyStyle(NULL, WS_VISIBLE);
	InvalidateRect(m_CRectCListCtrlInfo, TRUE);
}


void CChessCHSDlg::OnUserListShow()
{
	// TODO: 在此添加命令处理程序代码
	m_CListCtrlChess.ModifyStyle(WS_VISIBLE, NULL);
	m_CListCtrlUser.ModifyStyle(NULL, WS_VISIBLE);
	InvalidateRect(m_CRectCListCtrlInfo, TRUE);
}



BOOL CChessCHSDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if (WM_RBUTTONDOWN == pMsg->message)
	{
		//CWnd* pCWnd = (CWnd*)GetDlgItem(IDC_RICHEDIT2_MSG);
		CPoint point(pMsg->pt);
		ScreenToClient(&point);
		if (m_CRectCRichEditCtrlMsg.PtInRect(point)) // && pCWnd==GetFocus()
		{
			CMenu menu;
			menu.LoadMenu(IDR_POPUP_RICHCTRL);
			CMenu* pPopupMenu = menu.GetSubMenu(0);

			pPopupMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, pMsg->pt.x, pMsg->pt.y, this);

			return TRUE;
		}
	}

	return CDialogEx::PreTranslateMessage(pMsg);
}


void CChessCHSDlg::OnChatMsgClear()
{
	// TODO: 在此添加命令处理程序代码
	m_CRichEditCtrlMsg.SetSel(0, -1);
	m_CRichEditCtrlMsg.ReplaceSel(L"");
}
