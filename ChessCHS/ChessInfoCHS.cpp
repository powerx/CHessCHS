#include "StdAfx.h"
#include "ChessInfoCHS.h"

#include "resource.h"		// 主符号

namespace Wzz
{

	const CPoint CChessInfoCHS::s_CpChessPosition[9][10] = {
		{CPoint(269,101),CPoint(269,158),CPoint(269,215),CPoint(269,272),CPoint(269,329),CPoint(269,387),CPoint(269,444),CPoint(269,501),CPoint(269,558),CPoint(269,615)},
		{CPoint(326,101),CPoint(326,158),CPoint(326,215),CPoint(326,272),CPoint(326,329),CPoint(326,387),CPoint(326,444),CPoint(326,501),CPoint(326,558),CPoint(326,615)},
		{CPoint(383,101),CPoint(383,158),CPoint(383,215),CPoint(383,272),CPoint(383,329),CPoint(383,387),CPoint(383,444),CPoint(383,501),CPoint(383,558),CPoint(383,615)},
		{CPoint(440,101),CPoint(440,158),CPoint(440,215),CPoint(440,272),CPoint(440,329),CPoint(440,387),CPoint(440,444),CPoint(440,501),CPoint(440,558),CPoint(440,615)},
		{CPoint(497,101),CPoint(497,158),CPoint(497,215),CPoint(497,272),CPoint(497,329),CPoint(497,387),CPoint(497,444),CPoint(497,501),CPoint(497,558),CPoint(497,615)},
		{CPoint(554,101),CPoint(554,158),CPoint(554,215),CPoint(554,272),CPoint(554,329),CPoint(554,387),CPoint(554,444),CPoint(554,501),CPoint(554,558),CPoint(554,615)},
		{CPoint(611,101),CPoint(611,158),CPoint(611,215),CPoint(611,272),CPoint(611,329),CPoint(611,387),CPoint(611,444),CPoint(611,501),CPoint(611,558),CPoint(611,615)},
		{CPoint(668,101),CPoint(668,158),CPoint(668,215),CPoint(668,272),CPoint(668,329),CPoint(668,387),CPoint(668,444),CPoint(668,501),CPoint(668,558),CPoint(668,615)},
		{CPoint(725,101),CPoint(725,158),CPoint(725,215),CPoint(725,272),CPoint(725,329),CPoint(725,387),CPoint(725,444),CPoint(725,501),CPoint(725,558),CPoint(725,615)}
	};


	CChessInfoCHS::CChessInfoCHS(void)
		: m_bGameStart(false)
		, m_bGameSelf(true)
	{
		ClearChessInfo();
	}


	CChessInfoCHS::~CChessInfoCHS(void)
	{
	}


	bool CChessInfoCHS::IsChessRed(const CPoint point) const
	{
		if (CHESSTYPE_RED_BING>=m_ChessCHS[point.x][point.y]
			&& CHESSTYPE_NULL!=m_ChessCHS[point.x][point.y])
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool CChessInfoCHS::IsChessBlack(const CPoint point) const
	{
		if (CHESSTYPE_BLACK_JU <= m_ChessCHS[point.x][point.y])
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool CChessInfoCHS::IsChessNull(const CPoint point) const
	{
		if (CHESSTYPE_NULL == m_ChessCHS[point.x][point.y])
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	bool CChessInfoCHS::IsChessKing(const CPoint point) const
	{
		if (CHESSTYPE_RED_SHUAI==m_ChessCHS[point.x][point.y]
			|| CHESSTYPE_BLACK_JIANG==m_ChessCHS[point.x][point.y])
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	bool CChessInfoCHS::GetGameStart() const
	{
		return m_bGameStart;
	}

	void CChessInfoCHS::SetGameStart(const bool bGameStart)
	{
		m_bGameStart = bGameStart;
	}


	void CChessInfoCHS::ClearChessInfo(void)
	{
		m_bGameSelf = true;

		m_ChessCHSFocusStart = m_ChessCHSFocusStop = CPoint(-1, -1);

		for (int nX=0; nX<9; nX++)
		{
			for (int nY=0; nY<10; nY++)
			{
				m_ChessCHS[nX][nY] = CHESSTYPE_NULL;
			}
		}
	}

	// 初始化象棋棋子数据
	void CChessInfoCHS::InitChessInfo(void)
	{
		m_ChessCHSFocusStart = m_ChessCHSFocusStop = CPoint(-1, -1);

		for (int nX=0; nX<9; nX++)
		{
			for (int nY=0; nY<10; nY++)
			{
				m_ChessCHS[nX][nY] = CHESSTYPE_NULL;
			}
		}
		m_ChessCHS[0][0] = m_ChessCHS[8][0] = CHESSTYPE_BLACK_JU;
		m_ChessCHS[1][0] = m_ChessCHS[7][0] = CHESSTYPE_BLACK_MA;
		m_ChessCHS[2][0] = m_ChessCHS[6][0] = CHESSTYPE_BLACK_XIANG;
		m_ChessCHS[3][0] = m_ChessCHS[5][0] = CHESSTYPE_BLACK_SHI;
		m_ChessCHS[4][0] = CHESSTYPE_BLACK_JIANG;
		m_ChessCHS[1][2] = m_ChessCHS[7][2] = CHESSTYPE_BLACK_PAO;
		m_ChessCHS[0][3] = m_ChessCHS[2][3] = m_ChessCHS[4][3] = m_ChessCHS[6][3] = m_ChessCHS[8][3] = CHESSTYPE_BLACK_ZU;

		m_ChessCHS[0][9] = m_ChessCHS[8][9] = CHESSTYPE_RED_JU;
		m_ChessCHS[1][9] = m_ChessCHS[7][9] = CHESSTYPE_RED_MA;
		m_ChessCHS[2][9] = m_ChessCHS[6][9] = CHESSTYPE_RED_XIANG;
		m_ChessCHS[3][9] = m_ChessCHS[5][9] = CHESSTYPE_RED_SHI;
		m_ChessCHS[4][9] = CHESSTYPE_RED_SHUAI;
		m_ChessCHS[1][7] = m_ChessCHS[7][7] = CHESSTYPE_RED_PAO;
		m_ChessCHS[0][6] = m_ChessCHS[2][6] = m_ChessCHS[4][6] = m_ChessCHS[6][6] = m_ChessCHS[8][6] = CHESSTYPE_RED_BING;
	}

	void CChessInfoCHS::DrawAllChess(CDC* pDC)
	{
		CBitmap bitmap;
		BITMAP bit;
		CDC dcCompatible;
		dcCompatible.CreateCompatibleDC(pDC);// 创建一个兼容DC
		bitmap.LoadBitmap(MAKEINTRESOURCE(IDB_MAIN_CHESSCHS));
		bitmap.GetBitmap(&bit);
		CBitmap* pOldBitmap = dcCompatible.SelectObject(&bitmap);

		if (m_bGameStart)
		{
			if (m_bGameSelf)
			{
				// 设置位图显示时，白色显示为透明颜色;
				pDC->TransparentBlt(140, 470, bit.bmWidth/14, bit.bmHeight,
					&dcCompatible,	bit.bmWidth/14*CHESSTYPE_RED_SHUAI, 0, bit.bmWidth/14, bit.bmHeight, RGB(255,255,255));
			}
			else
			{
				pDC->TransparentBlt(140, 270, bit.bmWidth/14, bit.bmHeight,
					&dcCompatible,	bit.bmWidth/14*CHESSTYPE_BLACK_JIANG, 0, bit.bmWidth/14, bit.bmHeight, RGB(255,255,255));
			}
		}

		for (int nX=0; nX<9; nX++)
		{
			for (int nY=0; nY<10; nY++)
			{
				if (CHESSTYPE_NULL != m_ChessCHS[nX][nY])
				{
					pDC->TransparentBlt(s_CpChessPosition[nX][nY].x-bit.bmHeight/2, s_CpChessPosition[nX][nY].y-bit.bmHeight/2, bit.bmWidth/14, bit.bmHeight,
						&dcCompatible,	bit.bmWidth/14*m_ChessCHS[nX][nY], 0, bit.bmWidth/14, bit.bmHeight, RGB(255,255,255));
				}
			}
		}

		bitmap.DeleteObject();              //删除加载的位图资源对象
		bitmap.LoadBitmap(MAKEINTRESOURCE(IDB_MAIN_CHESSCHSFOCUS)); // 加载位图资源
		bitmap.GetBitmap(&bit);
		dcCompatible.SelectObject(&bitmap);

		// 绘制焦点
		if (-1!=m_ChessCHSFocusStart.x && -1!=m_ChessCHSFocusStart.y)
		{
			int nX = m_ChessCHSFocusStart.x;
			int nY = m_ChessCHSFocusStart.y;
			pDC->TransparentBlt(s_CpChessPosition[nX][nY].x-bit.bmHeight/2, s_CpChessPosition[nX][nY].y-bit.bmHeight/2, bit.bmWidth, bit.bmHeight,
				&dcCompatible,	0, 0, bit.bmWidth, bit.bmHeight, RGB(255,255,255));
		}
		if (-1!=m_ChessCHSFocusStop.x && -1!=m_ChessCHSFocusStop.y)
		{
			int nX = m_ChessCHSFocusStop.x;
			int nY = m_ChessCHSFocusStop.y;
			pDC->TransparentBlt(s_CpChessPosition[nX][nY].x-bit.bmHeight/2, s_CpChessPosition[nX][nY].y-bit.bmHeight/2, bit.bmWidth, bit.bmHeight,
				&dcCompatible,	0, 0, bit.bmWidth, bit.bmHeight, RGB(255,255,255));
		}

		bitmap.DeleteObject();

		dcCompatible.SelectObject(pOldBitmap);
	}

	CPoint CChessInfoCHS::TranslateCPoint(const CPoint pointChess)
	{
		return CPoint(8-pointChess.x, 9-pointChess.y);
	}

	CRect CChessInfoCHS::GetCRectChessCHS(const CPoint point)
	{
		int nX = point.x;
		int nY = point.y;
		return CRect(s_CpChessPosition[nX][nY].x-28, s_CpChessPosition[nX][nY].y-28
			, s_CpChessPosition[nX][nY].x+28, s_CpChessPosition[nX][nY].y+28);
	}


	CPoint CChessInfoCHS::GetCPointChessCHS(const CPoint point)
	{
		for (int nX=0; nX<9; nX++)
		{
			for (int nY=0; nY<10; nY++)
			{
				if (s_CpChessPosition[nX][nY].x-25<point.x
					&& s_CpChessPosition[nX][nY].x+25>point.x
					&& s_CpChessPosition[nX][nY].y-25<point.y
					&& s_CpChessPosition[nX][nY].y+25>point.y)
				{
					return CPoint(nX, nY);
				}
			}
		}

		return CPoint(-1, -1);
	}

	bool CChessInfoCHS::ChessMoveOrEat(const CPoint pointBegin, const CPoint pointEnd)
	{
		if (CanChessMoveOrEat(pointBegin, pointEnd))
		{
			const CPoint pointStartOld = m_ChessCHSFocusStart;
			const CPoint pointStopOld = m_ChessCHSFocusStop;
			m_ChessCHSFocusStart = pointBegin;
			m_ChessCHSFocusStop = pointEnd;
			m_ChessCHS[m_ChessCHSFocusStop.x][m_ChessCHSFocusStop.y] = m_ChessCHS[m_ChessCHSFocusStart.x][m_ChessCHSFocusStart.y];
			m_ChessCHS[m_ChessCHSFocusStart.x][m_ChessCHSFocusStart.y] = CHESSTYPE_NULL;
			
			AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(pointStartOld));
			AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(pointStopOld));
			AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(m_ChessCHSFocusStart));
			AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(m_ChessCHSFocusStop));
			AfxGetMainWnd()->InvalidateRect(CRect(140, 270, 196, 326));
			AfxGetMainWnd()->InvalidateRect(CRect(140, 470, 196, 526));

			OutputDebugString(L"ChessMoveOrEat Yes!");
			return true;
		}
		else
		{
			OutputDebugString(L"ChessMoveOrEat No!");
			return false;
		}
	}

	bool CChessInfoCHS::CanChessMoveOrEat(const CPoint pointBegin, const CPoint pointEnd) const
	{
		switch (m_ChessCHS[pointBegin.x][pointBegin.y])
		{
		case CHESSTYPE_BLACK_ZU: // 黑色“卒”的移动规则
		case CHESSTYPE_RED_BING: // 红色“兵”的移动规则
			return CanMoveChessBING(pointBegin, pointEnd);
			break;
		case CHESSTYPE_BLACK_PAO:
		case CHESSTYPE_RED_PAO:
			return CanMoveChessPAO(pointBegin, pointEnd);
			break;
		case CHESSTYPE_BLACK_JU:
		case CHESSTYPE_RED_JU:
			return CanMoveChessJU(pointBegin, pointEnd);
			break;
		case CHESSTYPE_BLACK_MA:
		case CHESSTYPE_RED_MA:
			return CanMoveChessMA(pointBegin, pointEnd);
			break;
		case CHESSTYPE_BLACK_XIANG:
		case CHESSTYPE_RED_XIANG:
			return CanMoveChessXIANG(pointBegin, pointEnd);
			break;
		case CHESSTYPE_BLACK_SHI:
		case CHESSTYPE_RED_SHI:
			return CanMoveChessSHI(pointBegin, pointEnd);
			break;
		case CHESSTYPE_BLACK_JIANG:
		case CHESSTYPE_RED_SHUAI:
			return CanMoveChessKING(pointBegin, pointEnd);
			break;
		default:
			break;
		}

		return false;
	}

	bool CChessInfoCHS::ChessClicked(const CPoint point)
	{
		CPoint pointChess = GetCPointChessCHS(point);
		if (CPoint(-1, -1) != pointChess)
		{
			if (CPoint(-1,-1) != m_ChessCHSFocusStart
				&& CPoint(-1,-1) == m_ChessCHSFocusStop)
			{
				if (IsChessRed(pointChess))
				{
					const CPoint pointStart = m_ChessCHSFocusStart;
					m_ChessCHSFocusStart = pointChess;
					AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(pointStart));
					AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(m_ChessCHSFocusStart));
				}
				else if (!IsChessRed(pointChess) && CanChessMoveOrEat(m_ChessCHSFocusStart, pointChess))
				{
					/*m_ChessCHSFocusStop = CPoint(nX, nY);
					m_ChessCHS[m_ChessCHSFocusStop.x][m_ChessCHSFocusStop.y] = m_ChessCHS[m_ChessCHSFocusStart.x][m_ChessCHSFocusStart.y];
					m_ChessCHS[m_ChessCHSFocusStart.x][m_ChessCHSFocusStart.y] = CHESSTYPE_NULL;
					m_bGameSelf = false;
					AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(m_ChessCHSFocusStart));
					AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(m_ChessCHSFocusStop));
					AfxGetMainWnd()->InvalidateRect(CRect(140, 270, 196, 326));
					AfxGetMainWnd()->InvalidateRect(CRect(140, 470, 196, 526));*/

					m_bGameSelf = false;
					ChessMoveOrEat(m_ChessCHSFocusStart, pointChess);

					return true;
				}
			}
			else
			{
				if (IsChessRed(pointChess))
				{
					const CPoint pointStart = m_ChessCHSFocusStart;
					const CPoint pointStop = m_ChessCHSFocusStop;
					m_ChessCHSFocusStart = pointChess;
					m_ChessCHSFocusStop = CPoint(-1, -1);
					m_bGameSelf = true;
					AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(m_ChessCHSFocusStart));
					AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(pointStart));
					AfxGetMainWnd()->InvalidateRect(GetCRectChessCHS(pointStop));
				}
			}
		}

		return false;
	}

	bool CChessInfoCHS::GetMySelfRun() const
	{
		return m_bGameSelf;
	}

	void CChessInfoCHS::SetMySelfRun(const bool bMySelfRun)
	{
		m_bGameSelf = bMySelfRun;
	}

	CPoint CChessInfoCHS::GetChessFocusStart() const
	{
		return m_ChessCHSFocusStart;
	}

	CPoint CChessInfoCHS::GetChessFocusStop() const
	{
		return m_ChessCHSFocusStop;
	}

	void CChessInfoCHS::SetChessFocusStart(const CPoint pointStart)
	{
		m_ChessCHSFocusStart = pointStart;
	}

	void CChessInfoCHS::SetChessFocusStop(const CPoint pointStop)
	{
		m_ChessCHSFocusStop = pointStop;
	}


	bool CChessInfoCHS::CanMoveChessJU(const CPoint pointBegin, const CPoint pointEnd) const
	{
		CPoint aCpBias = pointEnd - pointBegin;
		long X_Result = aCpBias.x>=0 ? aCpBias.x : -aCpBias.x;
		long Y_Result = aCpBias.y>=0 ? aCpBias.y : -aCpBias.y;
		
		if(0 == X_Result)
		{
			int y_big = pointEnd.y > pointBegin.y ? pointEnd.y : pointBegin.y;
			int y_smail = pointEnd.y < pointBegin.y ? pointEnd.y : pointBegin.y;
			for(int i=y_smail+1; i<y_big; i++)
			{
				if(!IsChessNull(CPoint(pointBegin.x,i)))
				{
					return false;
				}
			}
		}
		else if(0 == Y_Result)
		{
			int x_big = pointEnd.x > pointBegin.x ? pointEnd.x : pointBegin.x;
			int x_smail = pointEnd.x < pointBegin.x ? pointEnd.x : pointBegin.x;
			for(int i=x_smail+1; i<x_big; i++)
			{
				if(!IsChessNull(CPoint(i,pointBegin.y)))
				{
					return false;
				}
			}
		}
		else
		{
			return false;
		}
		//如果起点到终点之间相隔一个棋子，而且终点处是对方的棋子或者是空时，则能移动;
		if (IsChessRed(pointBegin) && !IsChessRed(pointEnd))
		{
			return true;
		}
		if (IsChessBlack(pointBegin) && !IsChessBlack(pointEnd))
		{
			return true;
		}

		return false;
	}

	bool CChessInfoCHS::CanMoveChessMA(const CPoint pointBegin, const CPoint pointEnd) const
	{
		CPoint aCpBias = pointEnd - pointBegin;
		long X_Result = aCpBias.x>=0 ? aCpBias.x : -aCpBias.x;
		long Y_Result = aCpBias.y>=0 ? aCpBias.y : -aCpBias.y;
		if((2==X_Result && 1==Y_Result && IsChessNull(CPoint((pointEnd.x+pointBegin.x)/2,pointBegin.y)))
			|| (1==X_Result && 2==Y_Result && IsChessNull(CPoint(pointBegin.x,(pointEnd.y+pointBegin.y)/2))))
		{
			if (IsChessRed(pointBegin) && !IsChessRed(pointEnd))
			{
				return true;
			}
			else if (IsChessBlack(pointBegin) && !IsChessBlack(pointEnd))
			{
				return true;
			}
		}

		return false;
	}

	bool CChessInfoCHS::CanMoveChessPAO(const CPoint pointBegin, const CPoint pointEnd) const
	{
		CPoint aCpBias = pointEnd - pointBegin;
		long X_Result = aCpBias.x>=0 ? aCpBias.x : -aCpBias.x;
		long Y_Result = aCpBias.y>=0 ? aCpBias.y : -aCpBias.y;
		int nChessNum = 0;
		if(0 == X_Result)
		{
			int y_big = pointEnd.y > pointBegin.y ? pointEnd.y : pointBegin.y;
			int y_smail = pointEnd.y < pointBegin.y ? pointEnd.y : pointBegin.y;
			for(int i=y_smail+1; i<y_big; i++)
			{
				if(!IsChessNull(CPoint(pointBegin.x,i)))
				{
					if (++nChessNum > 1)
					{
						return false;
					}
				}
			}
		}
		else if(0 == Y_Result)
		{
			int x_big = pointEnd.x > pointBegin.x ? pointEnd.x : pointBegin.x;
			int x_smail = pointEnd.x < pointBegin.x ? pointEnd.x : pointBegin.x;
			for(int i=x_smail+1; i<x_big; i++)
			{
				if(!IsChessNull(CPoint(i,pointBegin.y)))
				{
					if (++nChessNum > 1)
					{
						return false;
					}
				}
			}
		}
		else
		{
			return false;
		}
		//如果起点到终点之间相隔一个棋子，而且终点处是对方的棋子或者是空时，则能移动;
		if (0 == nChessNum)
		{
			if (IsChessRed(pointBegin) && IsChessNull(pointEnd))
			{
				return true;
			}
			else if (IsChessBlack(pointBegin) && IsChessNull(pointEnd))
			{
				return true;
			}
		}
		else if (1 == nChessNum)
		{
			if (IsChessRed(pointBegin) && !IsChessRed(pointEnd))
			{
				return true;
			}
			else if (IsChessBlack(pointBegin) && !IsChessBlack(pointEnd))
			{
				return true;
			}
		}

		return false;
	}

	bool CChessInfoCHS::CanMoveChessSHI(const CPoint pointBegin, const CPoint pointEnd) const
	{
		CPoint aCpBias = pointEnd - pointBegin;
		long X_Result = aCpBias.x>=0?aCpBias.x:-aCpBias.x;
		long Y_Result = aCpBias.y>=0?aCpBias.y:-aCpBias.y;
		if (IsChessBlack(pointBegin))
		{
			if((pointEnd.x>=3 && pointEnd.x<=5) && (pointEnd.y>=0 && pointEnd.y<=2) && (1==X_Result && 1==Y_Result)
				&& !IsChessBlack(pointEnd))
				return true;
		}
		else if (IsChessRed(pointBegin))
		{
			if((pointEnd.x>=3 && pointEnd.x<=5) && (pointEnd.y>=7 && pointEnd.y<=9) && (1==X_Result && 1 == Y_Result)
				&& !IsChessRed(pointEnd))
				return true;
		}

		return false;
	}

	bool CChessInfoCHS::CanMoveChessKING(const CPoint pointBegin,const CPoint pointEnd) const
	{
		CPoint aCpBias = pointEnd - pointBegin;
		long X_Result = aCpBias.x>=0?aCpBias.x:-aCpBias.x;
		long Y_Result = aCpBias.y>=0?aCpBias.y:-aCpBias.y;
		
		if (IsChessBlack(pointBegin))
		{
			if((pointEnd.x>=3 && pointEnd.x<=5) && (pointEnd.y>=0 && pointEnd.y<=2))
			{
				if(((1==X_Result && 0==Y_Result) || (0==X_Result && 1==Y_Result))
					&& !IsChessBlack(pointEnd))
				{
					return true; // （满足棋子活动范围的情况下）位移偏移量要准确，并且目标位置是空或者红子方能移动
				}
			}
		}
		else if (IsChessRed(pointBegin))
		{
			if((pointEnd.x>=3 && pointEnd.x<=5) && (pointEnd.y>=7 && pointEnd.y<=9))
			{
				if(((1==X_Result && 0==Y_Result) || (0==X_Result && 1==Y_Result))
					&& !IsChessRed(pointEnd))
				{
					return true; // （满足棋子活动范围的情况下）位移偏移量要准确，并且目标位置是空或者黑子方能移动
				}
			}
		}

		return false;
	}

	bool CChessInfoCHS::CanMoveChessBING(const CPoint pointBegin,const CPoint pointEnd) const
	{
		CPoint aCpBias = pointEnd - pointBegin;
		long X_Result = aCpBias.x>=0 ? aCpBias.x : -aCpBias.x;
		long Y_Result = aCpBias.y>=0 ? aCpBias.y : -aCpBias.y;
		
		if (IsChessRed(pointBegin))
		{
			if((pointBegin.y<=4) && (1==X_Result && 0==aCpBias.y) || (0==aCpBias.x && -1==aCpBias.y))
				return true;
			else if(pointBegin.y>=5 && 0==aCpBias.x && -1==aCpBias.y)
				return true;
		}
		else if (IsChessBlack(pointBegin))
		{
			if(pointBegin.y<=4 && 0==aCpBias.x && 1==aCpBias.y)
				return true;
			else if(pointBegin.y>=5 && (1==X_Result && 0==aCpBias.y) || (0==aCpBias.x && 1==aCpBias.y))
				return true;
		}

		return false;
	}

	bool CChessInfoCHS::CanMoveChessXIANG(const CPoint pointBegin, const CPoint pointEnd) const
	{
		CPoint aCpBias = pointEnd - pointBegin;
		long X_Result = aCpBias.x >=0 ? aCpBias.x : -aCpBias.x;
		long Y_Result = aCpBias.y >=0 ? aCpBias.y : -aCpBias.y;
		// X轴和Y轴，棋子的偏移量都为2，而且起始点和终点的中点是空的才能继续判断
		if(2==X_Result && 2==Y_Result
			&& IsChessNull(CPoint((pointBegin.x+pointEnd.x)/2,(pointBegin.y+pointEnd.y)/2)))
		{
			if (IsChessRed(pointBegin) && !IsChessRed(pointEnd) && (9>=pointEnd.y&&5<=pointEnd.y))
			{
				return true;
			}
			else if (IsChessBlack(pointBegin) && !IsChessBlack(pointEnd) && (4>=pointEnd.y&&0<=pointEnd.y))
			{
				return true;
			}
		}
		return false;
	}
}

