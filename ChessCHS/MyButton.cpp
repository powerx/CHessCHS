// MyButton.cpp : implementation file
//

#include "stdafx.h"
#include "ButtonCH.h"

/////////////////////////////////////////////////////////////////////////////
// CMyButton



CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoMin = {
    CPoint(940,14), BUTTONIMAGE_MIN,
    BUTTONKIND_MIN, BUTTONSTATUS_DEFAULT
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoClose = {
    CPoint(970, 14), BUTTONIMAGE_CLOSE,
    BUTTONKIND_CLOSE, BUTTONSTATUS_DEFAULT
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoInvatefriend = {
    CPoint(851, 14), BUTTONIMAGE_INVATEFRIEND,
    BUTTONKIND_INVATEFRIEND, BUTTONSTATUS_DEFAULT
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoExit = {
    CPoint(909, 91), BUTTONIMAGE_EXIT,
    BUTTONKIND_EXIT, BUTTONSTATUS_DEFAULT
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoTool = {
    CPoint(802, 91), BUTTONIMAGE_TOOL,
    BUTTONKIND_TOOL, BUTTONSTATUS_DEFAULT
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoDaTing = {
    CPoint(802,44), BUTTONIMAGE_DATING,
    BUTTONKIND_DATING, BUTTONSTATUS_DEFAULT
};
CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoConfig = {
    CPoint(909, 44), BUTTONIMAGE_CONFIG,
    BUTTONKIND_CONFIG, BUTTONSTATUS_DEFAULT
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoSend = {
    CPoint(971,696), BUTTONIMAGE_SEND,
    BUTTONKIND_SEND, BUTTONSTATUS_DEFAULT
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoDaPu = {
    CPoint(698,677), BUTTONIMAGE_DAPU,
    BUTTONKIND_DAPU, BUTTONSTATUS_DISABLE
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoHuiQi = {
    CPoint(583,677), BUTTONIMAGE_HUIQI,
    BUTTONKIND_HUIQI, BUTTONSTATUS_DISABLE
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoQuiHe = {
    CPoint(353,677), BUTTONIMAGE_QIUHE,
    BUTTONKIND_QIUHE, BUTTONSTATUS_DISABLE
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoStart = {
    CPoint(238,677), BUTTONIMAGE_START,
    BUTTONKIND_START, BUTTONSTATUS_DISABLE
};

CButtonCH::MyButtonInfo CButtonCH::s_MyButtonInfoRenShu = {
    CPoint(468,677), BUTTONIMAGE_RENSHU,
    BUTTONKIND_RENSHU, BUTTONSTATUS_DISABLE
};

CButtonCH::CButtonCH(const MyButtonInfo buttonInfo)
    : m_bTracking(false)
{
    m_MyButtonInfo = buttonInfo;
	if (BUTTONSTATUS_DISABLE == buttonInfo.buttonStatus)
	{
		m_bEnableButton = FALSE;
	}
	else
	{
		m_bEnableButton = TRUE;
	}
}

CButtonCH::~CButtonCH()
{
}


BEGIN_MESSAGE_MAP(CButtonCH, CButton)
	//{{AFX_MSG_MAP(CMyButton)
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_ERASEBKGND()
	//}}AFX_MSG_MAP
	ON_WM_TIMER()
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMyButton message handlers

void CButtonCH::OnMouseMove(UINT nFlags, CPoint point) 
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(!m_bTracking && m_bEnableButton)
	{
		m_bTracking=true;
		m_MyButtonInfo.buttonStatus = BUTTONSTATUS_OVER;

		CPoint	point;
		CRect	rect;
		GetWindowRect(&rect);	
		GetCursorPos(&point);
		if (!rect.PtInRect(point) && m_bTracking && m_bEnableButton)
		{
			SetTimer(1,10,NULL);
			return;
		}
		else
		{
			Invalidate(TRUE);
			SetTimer(1,10,NULL);
		}
	}
	
	CButton::OnMouseMove(nFlags, point);
}

void CButtonCH::PreSubclassWindow()
{
    ModifyStyle(0, BS_OWNERDRAW | WS_VISIBLE | WS_CHILD);
    EnableWindow();
    ModifyPlace();

	CButton::PreSubclassWindow();
}

void CButtonCH::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	UINT nItemState = lpDrawItemStruct->itemState; 
	if (nItemState & ODS_FOCUS)
	{
		if (nItemState & ODS_SELECTED)
		{ 
			m_MyButtonInfo.buttonStatus = BUTTONSTATUS_DOWN;
		}
		else
		{
			if(m_bTracking)
				m_MyButtonInfo.buttonStatus = BUTTONSTATUS_OVER;
		}
	}
	else
	{
		m_MyButtonInfo.buttonStatus = BUTTONSTATUS_DEFAULT;
	}
	if (nItemState & ODS_DISABLED)
	{
		m_MyButtonInfo.buttonStatus = BUTTONSTATUS_DISABLE;
		m_bEnableButton = false;
	}

	CDC ButtonDC;
    ButtonDC.Attach(lpDrawItemStruct->hDC); // 得到用于绘制按钮的DC
	ButtonDC.SelectStockObject(NULL_BRUSH);

    CBitmap bitmap;
    bitmap.LoadBitmap(MAKEINTRESOURCE(m_MyButtonInfo.buttonImage));

	BITMAP bit;
	bitmap.GetBitmap(&bit);                         // 获取当前位图的相关参数
	CRect rc(lpDrawItemStruct->rcItem);             // 获取按钮所占的矩形大小
	CDC dcCompatible;
	dcCompatible.CreateCompatibleDC(&ButtonDC);     // 创建一个兼容DC
	CBitmap* pOldBitmap = dcCompatible.SelectObject(&bitmap); // 将当前的位图资源选入到设备描述表中

	// 绘制当前按键
	ButtonDC.BitBlt(rc.left, rc.top
        , bit.bmWidth/m_MyButtonInfo.buttonKind
        , bit.bmHeight
        , &dcCompatible
        , (bit.bmWidth/m_MyButtonInfo.buttonKind)*m_MyButtonInfo.buttonStatus
        , 0
        , SRCCOPY);

	// 设置位图显示时，透明显示粉红色;
//	ButtonDC.TransparentBlt(0, 0, bit.bmWidth, bit.bmHeight, &dcCompatible, 0, 0, bit.bmWidth, bit.bmHeight, RGB(255,0,255));

	dcCompatible.SelectObject(pOldBitmap);          // 将以前的位图资源选回到设备描述表中
	bitmap.DeleteObject();                          // 释放位图资源
}

BOOL CButtonCH::OnEraseBkgnd(CDC* pDC) 
{
	return FALSE; // 要使得按钮不需要绘制默认背景颜色，可以重载当前消息处理函数，仅书写该句;
//	return CButton::OnEraseBkgnd(pDC);
}

//////////////////////////////////////////////////////////////////////////
// CButton Operation


BOOL CButtonCH::EnableWindow()
{
	if (m_bEnableButton) // 设置按键可用
	{
		m_MyButtonInfo.buttonStatus = BUTTONSTATUS_DEFAULT;
	}
	else
	{
		m_MyButtonInfo.buttonStatus = BUTTONSTATUS_DISABLE;
	}
	Invalidate(TRUE);
	return CButton::EnableWindow(m_bEnableButton);
}

BOOL CButtonCH::EnableWindow(BOOL bEnableButton)
{
	m_bEnableButton = bEnableButton;
	if (m_bEnableButton) // 设置按键可用
	{
		m_MyButtonInfo.buttonStatus = BUTTONSTATUS_DEFAULT;
	}
	else
	{
		m_MyButtonInfo.buttonStatus = BUTTONSTATUS_DISABLE;
	}

	Invalidate(TRUE);
	return CButton::EnableWindow(bEnableButton);
}


void CButtonCH::ModifyPlace()
{
    CPoint pointLeftTop(0, 0);
    CBitmap bitmap;

    bitmap.LoadBitmap(MAKEINTRESOURCE(m_MyButtonInfo.buttonImage));
    pointLeftTop.x = m_MyButtonInfo.buttonCPoint.x;
    pointLeftTop.y = m_MyButtonInfo.buttonCPoint.y;

    BITMAP bit;
    bitmap.GetBitmap(&bit);      // 获取当前位图的相关参数

    MoveWindow(pointLeftTop.x, pointLeftTop.y
        , bit.bmWidth/m_MyButtonInfo.buttonKind
        , bit.bmHeight);
}


void CButtonCH::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CPoint	point;
	CRect	rect;
	GetWindowRect(&rect);	
	GetCursorPos(&point);

	if (!rect.PtInRect(point) && m_bTracking && m_bEnableButton)
	{
		KillTimer(nIDEvent);
		m_bTracking = false;
		m_MyButtonInfo.buttonStatus = BUTTONSTATUS_DEFAULT;
		Invalidate(TRUE);
	}

	CButton::OnTimer(nIDEvent);
}
