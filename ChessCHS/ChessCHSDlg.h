
// ChessCHSDlg.h : 头文件
//

#pragma once

class CChessCHSDlgAutoProxy;


#include "ButtonCHS.h"
#include "SocketCHS.h"
#include "ChessInfoCHS.h"


// CChessCHSDlg 对话框
class CChessCHSDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CChessCHSDlg);
	friend class CChessCHSDlgAutoProxy;

// 构造
public:
	CChessCHSDlg(CWnd* pParent = NULL);	// 标准构造函数
	virtual ~CChessCHSDlg();

// 对话框数据
	enum { IDD = IDD_CHESSCHS_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	CChessCHSDlgAutoProxy* m_pAutoProxy;
	HICON m_hIcon;

	BOOL CanExit();

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	virtual void OnOK();
	virtual void OnCancel();
	DECLARE_MESSAGE_MAP()


	afx_msg void OnBnClickedButtonInvatefriend();
	afx_msg void OnBnClickedButtonClose();
	afx_msg void OnBnClickedButtonMin();
	afx_msg void OnBnClickedButtonStart();
	afx_msg void OnBnClickedButtonRenShu();
	afx_msg void OnBnClickedButtonExit();
	afx_msg void OnBnClickedButtonSend();
	afx_msg void OnBnClickedButtonQuihe();

	afx_msg void OnEnChangeEditText();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);

public:
	void OnUDPReceive(const Wzz::CUDPSocketCHS::UdpSocketType type, int nErrorCode);
	void OnTCPConnect(int nErrorCode);
	void OnTCPAccept(int nErrorCode);
	void OnTCPReceive(int nErrorCode);

public:
	afx_msg void OnNMRClickListUser(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnUserListConnect();
	afx_msg void OnUserListShuaXin();
	afx_msg void OnOptionConnect();
	afx_msg void OnChessListShow();
	afx_msg void OnUserListShow();
	afx_msg void OnNMRClickListChess(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnChatMsgClear();

private:
	void InitFaceImage();
	void InitCEditText();
	void InitCDialogFrame();
	void InitCButtonStatus();
	void InitCListCtrlInfo();
	void InitCRichEditCtrlMsg();
	void HideCaption() const;
	void ShowBackGround(CDC* pDC);
	void UpDateUserList(const IN_ADDR inAddr, const bool bLineOn); // 更新用户列表

private:
	bool InitCUDPSocketCHMultiCast(void);
	bool InitCUDPSocketCHBroadCast(void);
	bool InitCTCPSocketCH(void);

private:
	int m_nCurRecordPos;
	bool m_bGameConnect;
	CString m_CsUserNameYou;
	CString m_CsPCUserNameYou;
	CString m_CsUserNameSelf;
	CString m_CsPCUserNameSelf;

private:
	Wzz::CButtonCHS m_CMyButtonClose;
	Wzz::CButtonCHS m_CMyButtonMin;
	Wzz::CButtonCHS m_CMyButtonInvateFriend;
	Wzz::CButtonCHS m_CMyButtonConfig;
	Wzz::CButtonCHS m_CMyButtonDaTing;
	Wzz::CButtonCHS m_CMyButtonExit;
	Wzz::CButtonCHS m_CMyButtonTool;
	Wzz::CButtonCHS m_CMyButtonSend;

	Wzz::CButtonCHS m_CMyButtonDaPu;
	Wzz::CButtonCHS m_CMyButtonHuiQi;
	Wzz::CButtonCHS m_CMyButtonQiuHe;
	Wzz::CButtonCHS m_CMyButtonStart;
	Wzz::CButtonCHS m_CMyButtonRenShu;

	Wzz::CChessInfoCHS m_CChessInfoCHS;

	Wzz::CTCPSocketCHS m_CTCPSocketCHSServer;
	Wzz::CTCPSocketCHS m_CTCPSocketCHSClient;
	Wzz::CUDPSocketCHS m_CUDPSocketCHSMultiCast;
	Wzz::CUDPSocketCHS m_CUDPSocketCHSBroadCast;

private:
	CEdit          m_CEditText;
	CMenu          m_CMenuTray;
	CListCtrl      m_CListCtrlUser;
	CListCtrl      m_CListCtrlChess;
	CRichEditCtrl  m_CRichEditCtrlMsg;

private:
	const CRect m_CRectFaceImageMySelf;
	const CRect m_CRectFaceImageOpponent;
	const CRect m_CRectCEditText;
	const CRect m_CRectCRichEditCtrlMsg;
	const CRect m_CRectCListCtrlInfo;
	const CRect m_CRectCDialogFrame;
	const CRect m_CRectChessPosition;
	const CRect m_CRectChessTileRed;
	const CRect m_CRectChessTileBlack;
};
