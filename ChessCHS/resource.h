//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ChessCHS.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDS_ABOUTBOX                    101
#define IDD_CHESSCHS_DIALOG             102
#define IDP_RICHEDIT2_INIT_FAILED       102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDB_MAIN_BACKGROUND             129
#define IDB_MAIN_BUTTONBAOCUN           130
#define IDB_MAIN_BUTTONCLOSE            131
#define IDB_MAIN_BUTTONCONFIG           132
#define IDB_MAIN_BUTTONDAPU             133
#define IDB_MAIN_BUTTONDATING           134
#define IDB_MAIN_BUTTONEXIT             135
#define IDB_MAIN_BUTTONHUIQI            136
#define IDB_MAIN_BUTTONINVATEFRIEND     137
#define IDB_MAIN_BUTTONMIN              138
#define IDB_MAIN_BUTTONQIUHE            139
#define IDB_MAIN_BUTTONRENSHU           140
#define IDB_MAIN_BUTTONSEND             141
#define IDB_MAIN_BUTTONSTART            142
#define IDB_MAIN_BUTTONTOOL             143
#define IDB_MAIN_FACEDEFAULT            144
#define IDB_MAIN_CHESSCHS               145
#define IDB_MAIN_CHESSCHSFOCUS          146
#define IDB_BITMAP1                     147
#define IDB_POPUP_BACKGROUND            147
#define IDD_DIALOG_SET                  148
#define IDR_MENU_POPUPUSERLIST          149
#define IDR_POPUP_USERLIST              149
#define IDD_DIALOG_CONNECT              150
#define IDR_POPUP_RICHCTRL              151
#define IDC_BUTTON_CLOSE                1000
#define IDC_BUTTON_SEND                 1001
#define IDC_BUTTON_MIN                  1002
#define IDC_BUTTON_INVATEFRIEND         1003
#define IDC_BUTTON_CONFIG               1004
#define IDC_BUTTON_EXIT                 1005
#define IDC_BUTTON_DATING               1006
#define IDC_BUTTON_TOOL                 1007
#define IDC_BUTTON_START                1008
#define IDC_BUTTON_QUIHE                1009
#define IDC_BUTTON_RENSHU               1010
#define IDC_BUTTON_HUIQI                1011
#define IDC_BUTTON_DAPU                 1012
#define IDC_EDIT_TEXT                   1014
#define IDC_LIST_USER                   1015
#define IDC_RICHEDIT2_MSG               1016
#define IDC_FACE_OPPONENT               1017
#define IDC_FACE_MYSELF                 1018
#define IDC_IPADDRESS1                  1020
#define IDC_TAB1                        1021
#define IDC_LIST_CHESS                  1022
#define ID_32771                        32771
#define IDM_                            32772
#define IDM_USERLIST_CONNECT            32773
#define ID_32774                        32774
#define IDM_USERLIST_SHUAXIN            32775
#define ID_Menu                         32776
#define IDM_DIALOG_CONNECT              32777
#define IDM_OPTION_CONNECT              32778
#define ID_32779                        32779
#define ID_32780                        32780
#define IDM_USERLIST_SHOW               32781
#define IDM_CHESSLIST_SHOW              32782
#define ID_32783                        32783
#define IDM_MSG_CLEAR                   32784
#define IDM_CHATMSG_CLEAR               32785

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         32786
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
