# CHessCHS中国象棋  
[![star](https://gitee.com/HongXG/CHessCHS/badge/star.svg?theme=dark)](https://gitee.com/HongXG/CHessCHS/stargazers)
[![fork](https://gitee.com/HongXG/CHessCHS/badge/fork.svg?theme=dark)](https://gitee.com/HongXG/CHessCHS/members)  
目前仅支持局域网对战，能够通过局域网发现所有用户，并且显示在右侧的列表框中。用户可以自己选择对手方，并发起对战请求，对方同意，将开始局域网中国象棋对战服务。
![输入图片说明](https://gitee.com/HongXG/CHessCHS/raw/master/chesschs.jpg "在这里输入图片标题")